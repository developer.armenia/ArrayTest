package test.array;

import java.util.Arrays;
import java.util.Scanner;

public class Test {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        //get X and Y
        System.out.print("First line: two integers X and Y separated by one whitespace.");
        String XY = input.nextLine();
        if (XY == null || XY.trim().equals("")){
            System.out.println("Incorrect input and exist from program. Please insert X and with white space. For example 3 5");
            return;
        }
        String[] arrayXY = XY.split(" ", 2);
        int Y = Integer.valueOf(arrayXY[0]);
        int X = Integer.valueOf(arrayXY[1]);
        // init array
        int[] listArray = new int[Y+1];
        /* no needed. java already init primitive type array with 0
        for(int c = 0; c < Y; c++){
            listArray[c] = 0;
        }*/

        //get array list values
        int max = 0;
        for(int z = 0; z < X; z++){
            System.out.println("Next Y lines contain three numbers i, j and k separated by one whitespace");
            String line = input.nextLine();
            String[] lineXY = line.split(" ", 3);
            int i = Integer.valueOf(lineXY[0]);
            int j = Integer.valueOf(lineXY[1]);
            int k = Integer.valueOf(lineXY[2]);
            for (int m = i; m <=j ; m++) {
                listArray[m] += k;
                if (listArray[m] > max){
                    max = listArray[m];
                }
            }
        }
        System.out.println("max =" + max);
        /* no needed we alredy define max value during input data, otherwise we just can sort array and get latest element
        Arrays.sort(listArray);
        System.out.println("max =" + listArray[Y]);*/
        input.close();
    }
}
